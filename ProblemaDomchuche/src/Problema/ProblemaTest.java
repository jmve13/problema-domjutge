package Problema;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ProblemaTest {

	@Test
	void test() {
		assertEquals("Avui no hi ha projectils", Problema.multiple(2));
		assertEquals("Millor portar casc a classe", Problema.multiple(4));
		assertEquals("Avui no hi ha projectils", Problema.multiple(7));
		assertEquals("Millor portar casc a classe", Problema.multiple(12));
	}
	@Test
	void testOcult() {
		assertEquals("Avui no hi ha projectils", Problema.multiple(3));
		assertEquals("Millor portar casc a classe", Problema.multiple(40));
		assertEquals("Avui no hi ha projectils", Problema.multiple(22));
		assertEquals("Avui no hi ha projectils", Problema.multiple(11));
	}


}
